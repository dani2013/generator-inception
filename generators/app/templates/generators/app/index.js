const Generator = require('yeoman-generator');

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.option('use-npm', {
      type: Boolean,
      default: false,
      desc: 'Use npm package manager instead of yarn',
    });
  }

  install() {
    if (this.options['use-npm']) {
      this.npmInstall();
    } else {
      this.yarnInstall();
    }
  }
};
