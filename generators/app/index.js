const path = require('path');
const Generator = require('yeoman-generator');
const askName = require('inquirer-npm-name');
const kebabCase = require('lodash.kebabcase');

function makeGeneratorName(generatorName) {
  let name = generatorName.trim();
  name = kebabCase(generatorName);
  name = name.indexOf('generator-') === 0 ? name : 'generator-' + name;
  return name;
}

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.option('use-npm', {
      type: Boolean,
      default: false,
      desc: 'Use npm package manager instead of yarn',
    });

    this.option('name', {
      type: String,
      default: '',
      desc: 'Name of the generator',
    });
  }

  initializing() {
    this.props = {};
  }

  prompting() {
    return askName(
      {
        name: 'name',
        message: 'Generator name',
        default: makeGeneratorName(path.basename(process.cwd())),
        filter: makeGeneratorName,
        validate: str => {
          return str.length > 'generator-'.length;
        },
      },
      this
    ).then(props => {
      this.props.name = props.name;
      return this.prompt([
        {
          type: 'input',
          name: 'version',
          message: 'Version',
          default: '1.0.0',
        },
        {
          type: 'input',
          name: 'description',
          message: 'Description',
          default: answers => answers.name,
        },
        {
          type: 'input',
          name: 'author',
          message: 'Author',
          default: this.user.git.name(),
        },
        {
          type: 'input',
          name: 'keywords',
          message: 'Keywords',
          default: 'yeoman,generator',
        },
        {
          type: 'input',
          name: 'license',
          message: 'License',
          default: 'ISC',
        },
      ]).then(answers => {
        this.props.version = answers.version;
        this.props.description = answers.description;
        this.props.author = answers.author;
        this.props.keywords = answers.keywords.split(',').map(keyword => keyword.trim());
        this.props.license = answers.license;
      });
    });
  }

  paths() {
    this.destinationRoot(this.destinationPath(this.props.name));
  }

  writing() {
    this.fs.copyTpl(
      this.templatePath('.vscode/settings.json'),
      this.destinationPath('.vscode/settings.json')
    );
    this.fs.copyTpl(
      this.templatePath('generators/app/index.js'),
      this.destinationPath('generators/app/index.js')
    );
    this.fs.copyTpl(this.templatePath('.eslintignore'), this.destinationPath('.eslintignore'));
    this.fs.copyTpl(this.templatePath('.eslintrc'), this.destinationPath('.eslintrc'));
    this.fs.copyTpl(this.templatePath('.gitignore'), this.destinationPath('.gitignore'));
    this.fs.copyTpl(this.templatePath('.prettierignore'), this.destinationPath('.prettierignore'));
    this.fs.copyTpl(this.templatePath('.prettierrc'), this.destinationPath('.prettierrc'));
    this.fs.copyTpl(this.templatePath('README.md'), this.destinationPath('README.md'), {
      name: this.props.name,
      description: this.props.description,
      license: this.props.license,
    });
    this.fs.copyTpl(this.templatePath('package.json'), this.destinationPath('package.json'), {
      name: this.props.name,
      version: this.props.version,
      description: this.props.description,
      keywords: this.props.keywords,
      license: this.props.license,
    });
  }

  install() {
    if (this.options['use-npm']) {
      this.npmInstall();
    } else {
      this.yarnInstall();
    }
  }
};
